# atgem

This is an R package which provides crosswalks for boundary changes of Austrian municipalities
for the period 1984-2019.


## Installation

```R
install.packages("devtools")
devtools::install_gitlab("matmo/atgem")
library(atgem)
help(crosswalk)
```
