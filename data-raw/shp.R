path <- "data-raw/shp"
shp <- list()
for(s in list.files(path, pattern="*.shp"))
    shp[[s]] <- read_sf(paste0(path, "/", s)) %>% setNames(tolower(names(.)))
names(shp) <- substr(names(shp), 23, 26)
shp <- lapply(shp, st_snap_to_grid, size=0.1)
shp <- lapply(shp, st_make_valid)
#save(shp, data/shp.rda, compress="xz")

crs <- st_crs(shp[[1]])
bev <- list()
for(s in list.files(paste0(path, "/BEV"), pattern="*.shp$"))
    bev[[s]] <- read_sf(paste0(path, "/BEV/", s)) %>% setNames(tolower(names(.)))
names(bev) <- substr(names(bev), 4, 7)
bev <- lapply(bev, st_transform, crs=crs) 
bev <- lapply(bev, st_snap_to_grid, size=0.1)
bev <- lapply(bev, st_make_valid)
bev[["2004"]]$id <- bev[["2004"]]$hl4
bev[["2004"]]$name <- NA
bev[["2005"]] <- NULL
bev[["2007"]] <- bev[["2007"]] %>% group_by(pgnr) %>% summarise(id=first(pgnr), name=first(pgname))
bev[["2009"]] <- bev[["2009"]] %>% group_by(pgnr) %>% summarise(id=first(pgnr), name=first(pgname))

addVIE <- function(shn, src=shp[["2019"]]) {
    if((st_crs(shn)$epsg!=st_crs(src)$epsg))
        stop("CRS does not match")
    shn_new <- shn %>% filter(id!="90001") %>% select(id, name, geometry) %>% rbind(filter(src, substr(id, 1, 1)=="9"))
    st_make_valid(shn_new)
}

bev <- lapply(bev, addVIE)

shp <- c(bev, shp)
usethis::use_data(shp, overwrite=TRUE, internal=TRUE)
